#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include<cuda.h>
#include <opencv2\core\core.hpp>
#include<opencv2\highgui\highgui.hpp>
#include<opencv2\imgproc\imgproc.hpp>
//#include<opencv2\imgcodecs\imgcodecs.hpp>
#include<opencv2\video\video.hpp>
#include <stdio.h>
#include<iostream>

using namespace cv;
using namespace std;

void convertFrameToGrayScaleCPU(uchar *datosFrameEntrada, uchar *datosFrameSalida, int width, int height);
__global__ void convertFrameToGrayScaleGPU(uchar *datosFrameEntrada, uchar *datosFrameSalida, int width, int height);

int main(int argc, char **argv)
{
	/*if (argc != 3)
	{
	printf("[ERROR] Usage: %s Input.avi\n", argv[0]);
	exit(0);
	}*/

	VideoCapture videoentrada("mivideo.mp4");
	if (!videoentrada.isOpened())
	{
		printf("[ERROR] Couldn't open file %s as input video\n", argv[0]);
		exit(0);
	}
	int width = videoentrada.get(CV_CAP_PROP_FRAME_WIDTH);
	int height = videoentrada.get(CV_CAP_PROP_FRAME_HEIGHT);
	Size videoSize(width, height);

	VideoWriter videosalida(string("MiVideoSalidaGPU.mp4"),
		CV_FOURCC('M', 'P', '4', 'V'),
		videoentrada.get(CV_CAP_PROP_FPS),
		videoSize);



	if (!videosalida.isOpened())
	{
		printf("[ERROR] Couldn't open file %s as input video\n", argv[2]);
		videoentrada.release();
		exit(0);
	}

	Mat framecapturado;
	Mat frameSalida;
	uchar *datosFrameSalida = (uchar *)malloc(width*height * 3 * sizeof(uchar));
	uchar *d_datosframecapturado, *d_datosframesalida;
	cudaMalloc((void **)&d_datosframecapturado, sizeof(uchar)*width*height * 3);
	cudaMalloc((void **)&d_datosframesalida, sizeof(uchar)*width*height * 3);
	while (videoentrada.read(framecapturado))
	{
		cudaMemcpy(d_datosframecapturado, framecapturado.data, sizeof(uchar)*height*width * 3, cudaMemcpyHostToDevice);


		dim3 nBlock(32);
		dim3 nGrid(ceil((float)height*(float)width / 32.0));
		//convertFrameToGrayScaleCPU(framecapturado.data,datosFrameSalida,width,height);
		convertFrameToGrayScaleGPU <<<nGrid, nBlock >>> (d_datosframecapturado, d_datosframesalida, width, height);
		cudaDeviceSynchronize();

		cudaMemcpy(datosFrameSalida, d_datosframesalida, sizeof(uchar)*height*width * 3, cudaMemcpyDeviceToHost);

		frameSalida = Mat(videoSize, CV_8UC3, datosFrameSalida);
		videosalida.write(frameSalida);
	}


	cudaFree(d_datosframecapturado);
	cudaFree(d_datosframesalida);
	free(datosFrameSalida);
	videoentrada.release();
	videosalida.release();

	return(0);
}

void convertFrameToGrayScaleCPU(uchar *datosFrameEntrada, uchar *datosFrameSalida, int width, int height)
{
	for (int i = 0; i<height; i++)
	for (int j = 0; j < width; j++)
	{
		size_t pixelIdx = 3 * (i*width + j);
		uchar b = datosFrameEntrada[pixelIdx];
		uchar g = datosFrameEntrada[pixelIdx + 1];
		uchar r = datosFrameEntrada[pixelIdx + 2];
		uchar val = round((float)r*0.21 + (float)g*0.72 + (float)b*0.07);
		datosFrameSalida[pixelIdx] = val;
		datosFrameSalida[pixelIdx + 1] = val;
		datosFrameSalida[pixelIdx + 2] = val;
	}
}

__global__ void convertFrameToGrayScaleGPU(uchar *datosFrameEntrada, uchar *datosFrameSalida, int width, int height)
{
	size_t pixelIdx = 3 * (blockIdx.x*blockDim.x + threadIdx.x);
	if (pixelIdx >= width*height * 3)return;
	uchar b = datosFrameEntrada[pixelIdx];
	uchar g = datosFrameEntrada[pixelIdx + 1];
	uchar r = datosFrameEntrada[pixelIdx + 2];
	uchar val = round((float)r*0.21 + (float)g*0.72 + (float)b*0.07);
	datosFrameSalida[pixelIdx] = val;
	datosFrameSalida[pixelIdx + 1] = val;
	datosFrameSalida[pixelIdx + 2] = val;
}